defmodule Constants do

  def version, do: "1.0"
  def forkWindowSize, do: 10

  def sandboxTxSize, do: 200
  def sandboxIdSize, do: 5000
  def sandboxMemberSize, do: 5000

end
